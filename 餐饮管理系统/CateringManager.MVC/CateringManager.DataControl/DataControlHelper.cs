﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CateringManager.DataControl
{
    /// <summary>
    /// 用于操作和处理数据库得到的数据集
    /// </summary>
    public class DataControlHelper
    {
        /// <summary>
        /// 获取数据库链接
        /// </summary>
        /// <returns></returns>
        private SqlConnection GetConnection()
        {
            try
            {
                ConnectionHelper connection = new ConnectionHelper();
                SqlConnection conn = new SqlConnection(connection.GetConnectionString());
                return conn;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //  执行存储过程，不返回值。
        //  paraValues: 参数值列表。
        //  return: void
        public bool ExecuteNoQuery(string _name, params object[] paraValues)
        {
            using (SqlConnection con = GetConnection())
            {
                con.Open();
                SqlCommand comm = new SqlCommand(_name, con);
                comm.CommandType = CommandType.StoredProcedure;
                AddInParaValues(_name,comm, paraValues);
                int rowcount= comm.ExecuteNonQuery();
                return rowcount > 0 ? true : false;
            }
        }

        // 执行存储过程返回一个表。
        // paraValues: 参数值列表。
        // return: DataTable
        public DataTable ExecuteDataTable(string _name, params object[] paraValues)
        {
            using (SqlConnection conn = GetConnection())
            {                
                using (SqlCommand comm = new SqlCommand(_name, conn))
                {
                    comm.CommandType = CommandType.StoredProcedure;
                    AddInParaValues(_name,comm, paraValues);
                    conn.Open();
                    using (SqlDataAdapter sda = new SqlDataAdapter(comm))
                    {
                        DataTable dt = new DataTable();
                        sda.Fill(dt);
                        return dt;
                    }
                }
            }
        }

        // 执行存储过程分页获取一个表。
        // paraValues: 参数值列表。
        // return: DataTable
        public DataTable ExecuteDataTable<T>(string _name,out int pagecount,out int rowcount , params object[] paraValues)
        {
            using (SqlConnection conn = GetConnection())
            {
                using (SqlCommand comm = new SqlCommand(_name, conn))
                {
                    comm.CommandType = CommandType.StoredProcedure;
                    AddInParaValues(_name, comm, paraValues);
                    conn.Open();
                    using (SqlDataAdapter sda = new SqlDataAdapter(comm))
                    {
                        DataTable dt = new DataTable();
                        sda.Fill(dt);
                        pagecount = (int)comm.Parameters["@pageCount"].Value;
                        rowcount = (int)comm.Parameters["@recordCount"].Value;
                        return dt;
                    }
                }
            }
        }
        // 执行存储过程，返回SqlDataReader对象，
        // 在SqlDataReader对象关闭的同时，数据库连接自动关闭。
        // paraValues: 要传递给给存储过程的参数值类表。
        // return: SqlDataReader
        public SqlDataReader ExecuteDataReader(string _name, params object[] paraValues)
        {
            using (SqlConnection conn = GetConnection())
            {
                using (SqlCommand comm = new SqlCommand(_name, conn))
                {
                    comm.CommandType = CommandType.StoredProcedure;
                    AddInParaValues(_name,comm, paraValues);
                    conn.Open();
                    return comm.ExecuteReader(CommandBehavior.CloseConnection);
                }
            }
        }
        
        // 为 SqlCommand 添加参数及赋值。
        private void AddInParaValues(string _name, SqlCommand comm, params object[] paraValues)
        {
            //comm.Parameters.Add(new SqlParameter("@RETURN_VALUE", SqlDbType.Int));
            //comm.Parameters["@RETURN_VALUE"].Direction =
            //               ParameterDirection.ReturnValue;
            if (paraValues != null)
            {
                int length = paraValues.Length;
                for (int i = 0; i < length; i++)
                {
                    comm.Parameters.Add(paraValues[i]);
                }
            }
        }
    }
}
