﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.SqlClient;
using System.Collections;

namespace CateringManager.DataControl
{
    public class ConnectionHelper
    {
       Hashtable hTable= new Hashtable();

        /// <summary>
        /// 将数据库链接字符串读出来放入缓存中
        /// </summary>
        private void SetConnectionString()
        {
            try
            {
                string getConnStr = ConfigurationManager.ConnectionStrings["CateringManagerConnectionString"].ConnectionString;
                hTable.Add("connStr", getConnStr);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// 从缓存中取出数据库链接字符串
        /// </summary>
        /// <returns></returns>
        public string GetConnectionString()
        {
            if (hTable.Contains("connStr"))
            {
                return (string)hTable["connStr"];
            }
            else
            {
                SetConnectionString();
                return (string)hTable["connStr"];
            }
        }
    }
}
