﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CateringManager.Common
{
    /// <summary>
    /// 用于两个模型类之间的属性映射
    /// </summary>
    public class ModelesMapping
    {
        /// <summary>
        /// 将两个模型类之间的属性进行映射
        /// </summary>
        /// <typeparam name="R">映射后返回的类型</typeparam>
        /// <typeparam name="T">要映射的模型类</typeparam>
        /// <param name="model">要映射的模型类实体</param>
        /// <returns></returns>
        public static R Mapping<R, T>(T model)
        {
            R result = Activator.CreateInstance<R>();
            foreach (PropertyInfo info in typeof(R).GetProperties())
            {
                PropertyInfo pro = typeof(T).GetProperty(info.Name);
                if (pro != null)
                    info.SetValue(result, pro.GetValue(model));
            }
            return result;
        }
    }
}
