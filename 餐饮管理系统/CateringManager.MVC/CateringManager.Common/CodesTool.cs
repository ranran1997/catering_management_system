﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CateringManager.Common
{
    /// <summary>
    /// 号码工具类，用于生成各种号码（验证码、订到号等）
    /// </summary>
    public class CodesTool
    {
        /// <summary>
        /// 生成随机验证码
        /// </summary>
        /// <param name="validateCodeLength"></param>
        /// <returns></returns>
        public static string getValidateCode(int validateCodeLength)
        {
            // 定义可能出现的所有字符串,实际应用中有些字符很难区分,如0和o，可以去掉。
            string allChars = @"0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
            // 获取所有字符的长度
            int charsLength = allChars.Length;

            string validateCode = string.Empty;
            // 声明随机数生成器
            Random ran = new Random();
            //循环的次数代表生成的随机字符串的长度
            for (int i = 0; i < validateCodeLength; i++)
            {
                //将随机产生的数字作为字符串的索引,从而可以获得其下标所在的字符,并将这个字符加到随机字符串上
                validateCode += allChars[ran.Next(charsLength)];
            }
            //返回字符串
            return validateCode;
        }
        /// <summary>
        /// 根据身份证号算出性别、年龄、出生年月日
        /// </summary>
        /// <param name="idNumber"></param>
        /// <returns></returns>
        public static string[] GetBirthdaySexAgeByIdNumber(string idNumber)
        {
            //获取生日
            string birthday = idNumber.Length == 15 ? "19" + idNumber.Substring(6, 2) + "-" + idNumber.Substring(8, 2) + "-" + idNumber.Substring(10, 2) : idNumber.Substring(6, 4) + "-" + idNumber.Substring(10, 2) + "-" + idNumber.Substring(12, 2);
            DateTime dtBirthday = DateTime.Parse(birthday);
            TimeSpan sp = DateTime.Now.Subtract(dtBirthday);
            string age = (sp.TotalDays / 365).ToString("F0");
            string sex= idNumber.Substring(12, 3);
            if (int.Parse(sex) % 2 == 0)//性别代码为偶数是女性奇数为男性
            {
                sex = "false";
            }
            else
            {
                sex = "true";
            }
            string[] bsg = new string[] { birthday, sex, age };
            return bsg;
        }
        /// <summary>
        /// 生成部门编号，随机数
        /// </summary>
        /// <param name="letter"></param>
        /// <returns></returns>
        public static string CreateDpartmentNumber(string letter)
        {
            Random rand = new Random((int)DateTime.Now.Ticks);
            int number = rand.Next(1000,9999);
            return letter + number.ToString();
        }
    }
}
