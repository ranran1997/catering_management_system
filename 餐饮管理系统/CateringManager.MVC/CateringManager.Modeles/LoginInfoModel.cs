﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CateringManager.Modeles
{/// <summary>
/// 管理员登录表
/// </summary>
 public   class LoginInfoModel
    {/// <summary>
    /// 账号
    /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// 密码
        /// </summary>
        public string UserPass { get; set; }
        public DateTime AddTime { get; set; }
    
    }
}
