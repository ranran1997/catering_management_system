﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CateringManager.Modeles
{/// <summary>
/// 账单表
/// </summary>
   public class BillModel
    {/// <summary>
    /// 桌号
    /// </summary>
        public int TableID { get; set; }
        /// <summary>
        /// 客户编号
        /// </summary>
        public int CilentID { get; set; }
        public DateTime AddTime { get; set; }
    }
}
