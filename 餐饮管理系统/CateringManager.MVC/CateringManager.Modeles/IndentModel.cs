﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CateringManager.Modeles
{/// <summary>
/// 订单表
/// </summary>
   public class IndentModel
    {/// <summary>
    /// 菜名
    /// </summary>
        public string MenuName { get; set; }
        /// <summary>
        /// 单价
        /// </summary>
        public decimal Money { get; set; }
        /// <summary>
        /// 数量
        /// </summary>
        public int Number { get; set; }
        /// <summary>
        /// 小计
        /// </summary>
        public decimal Subtotal { get; set; }
        /// <summary>
        /// 桌号
        /// </summary>
        public int TableID { get; set; }
        public DateTime AddTime { get; set; }
    }
}
