﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CateringManager.Modeles
{/// <summary>
/// 订单账单关系表
/// </summary>
   public class IndentRBillModel
    {/// <summary>
/// 订单编号
/// </summary>
        public int IndentID { get; set; }
        /// <summary>
        /// 账单编号
        /// </summary>
        public int BillID { get; set; }
        public DateTime AddTime { get; set; }
    }
}
