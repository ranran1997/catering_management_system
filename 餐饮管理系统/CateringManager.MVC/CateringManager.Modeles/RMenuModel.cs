﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CateringManager.Modeles
{/// <summary>
/// /点菜表
/// </summary>
   public class RMenuModel
    {
    /// <summary>
    /// 菜单编号
    /// </summary>
        public int MenuID { get; set; }
        /// <summary>
        /// 客户编号
        /// </summary>
        public int CilentID { get; set; }
        /// <summary>
        /// 桌位编号
        /// </summary>
        public int DeskID { get; set; }
        /// <summary>
        /// 是否上菜
        /// </summary>
        public int Serving { get; set; }
        
        public DateTime AddTime { get; set; }

    }
}
