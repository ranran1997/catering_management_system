﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CateringManager.Modeles
{
   public class DeskModel
    {/// <summary>
    /// 桌号
    /// </summary>
        public int TableID { get; set; }
        /// <summary>
        /// 添加事件
        /// </summary>
        public DateTime AddTime { get; set; }
    }
}
