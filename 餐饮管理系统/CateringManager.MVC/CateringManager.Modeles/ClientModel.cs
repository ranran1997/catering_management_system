﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CateringManager.Modeles
{/// <summary>
/// 客户表
/// </summary>
  public  class ClientModel
    {/// <summary>
    /// 手机号
    /// </summary>
        public string Tel { get; set; }
        /// <summary>
        /// 添加时间
        /// </summary>
        public DateTime AddTime { get; set; }
    }
}
