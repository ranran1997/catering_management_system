﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CateringManager.Modeles
{/// <summary>
 /// 菜单表
 /// </summary>
    public class MenuModel
    {/// <summary>
     /// 名称
     /// </summary>
        public string MenuName { get; set; }
        /// <summary>
        /// 菜品类型
        /// </summary>
        public string Type { get; set; }
        /// <summary>
        /// 菜品单价
        /// </summary>
        public decimal Money { get; set; }
        /// <summary>
        /// 菜品描述
        /// </summary>
        public string Describe { get; set; }
        /// <summary>
        /// 菜品图片
        /// </summary>
        public string  Picture { get; set; }
        /// <summary>
        /// 是否销售
        /// </summary>
        public bool Market { get; set; }
        /// <summary>
        /// 菜品添加时间
        /// </summary>
        public DateTime AddTime { get; set; }
    }
}
