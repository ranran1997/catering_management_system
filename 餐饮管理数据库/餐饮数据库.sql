use master
go

create  database Catering --创建数据库 Catering
go
use Catering
go

create table Client     --创建表 客户表
(
ClientID int primary key identity(1,1),  --客户编号
Tel varchar(11) not null,                --手机号
AddTime datetime default(getdate())      --添加时间
)
--添加客户表数据
insert into Client values('152037724569',default)
insert into Client values('152037894569',default)
insert into Client values('152457724569',default)
insert into Client values('152037724659',default)
insert into Client values('152037724568',default)
insert into Client values('152037724567',default)


create table Menu        --创建表 菜单表
(
MenuID int primary key identity(1,1),     --菜单编号
MenuName varchar(10) not null,            --菜单名称
[Type] varchar(10) not null,              --菜系
[Money] money  not null,                  --单价
Describe varchar(100) not null,           --菜品描述
Picture	varchar(100) not null,            --图片
Market bit not null,                      --是否销售 1:是 0:否
AddTime datetime default(getdate())       --添加时间
)
--添加菜单表数据
insert into Menu values('土豆丝','热菜',12,'好吃吧','图片链接地址',1,default)
insert into Menu values('鱼香肉丝','热菜',12,'好吃吧','图片链接地址',1,default)
insert into Menu values('酸菜鱼','热菜',30,'好吃吧','图片链接地址',1,default)
insert into Menu values('宫保鸡丁','热菜',15,'好吃吧','图片链接地址',1,default)


create table Desk       --创建表   桌位表
(
DeskID int primary key identity(1,1),    --桌位编号
TableID int not null,                    --桌号
AddTime datetime default(getdate())      --添加时间
)
--添加桌位表数据
insert into Desk values(101,default)
insert into Desk values(102,default)
insert into Desk values(103,default)
insert into Desk values(104,default)
insert into Desk values(105,default)
insert into Desk values(201,default)
insert into Desk values(202,default)


create table RMenu     --创建表   点菜表
( 
ID int primary key identity(1,1),        --编号
MenuID int not null,                     --菜单编号
CilentID int not null,                   --客户编号
DeskID int not null,                     --桌位编号
Serving bit not null,                    --是否上菜 1:是 2: 否
AddTime datetime default(getdate())      --添加时间
)
--添加点菜表数据
insert into RMenu values(1,1,1,1,default)
insert into RMenu values(1,1,1,1,default)
insert into RMenu values(1,1,1,1,default)
insert into RMenu values(1,1,1,1,default)
insert into RMenu values(1,1,1,1,default)

create table LoginInfo   --创建表  登录表
(
ID  int primary key identity(1,1),      --编号
UserName varchar(20) not null,          --账号
UserPass varchar(20) not null,          --密码
AddTime datetime default(getdate())     --添加时间
)
--添加登录表数据
 insert into LoginInfo values('管理员','123456',default)


 create table Indent --创建表 订单表
 (
IndentID int primary key identity(1,1),  --编号
MenuName varchar(20)	not null,        --菜单名称
[Money]  Money not null,                 --单价
Number int  not null,                    --数量
Subtotal Money not null,                 --小计
TableID  int not null,                   --桌号
AddTime  datetime default(getdate())     --添加时间
 )
 --添加订单表数据
 insert into Indent values('土豆丝',12,1,12,101,default)

 create table  Bill    --创建表  账单表
 (
 BillID  int primary key identity(1,1),  --编号
 TableID int not null,                   --桌号
 CilentID int not null,                  --客户编号          
 AddTime  datetime default(getdate())    --添加时间
 )
 --添加账单表数据
 insert  into Bill values(101,1,default)

 create table IndentRBill   --订单账单关系表
 (
 ID int primary key identity(1,1),       --编号
 IndentID  int not null,                 --订单编号 
 BillID   int not null,                  --账单编号
 AddTime  datetime default(getdate())    --添加时间
 )
 --添加订单账单关系表
 insert into IndentRBill values(1,1,default)